/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop4k.functional.web.view

import com.samskivert.mustache.Mustache.Compiler
import org.slf4j.LoggerFactory
import org.springframework.core.io.Resource
import org.springframework.http.MediaType
import org.springframework.web.reactive.result.view.AbstractUrlBasedView
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.io.Reader
import java.util.*

/**
 * Spring WebFlux [View] using the Mustache template engine.
 *
 * @author debop
 * @since 2018. 2. 18.
 */
class MustacheView : AbstractUrlBasedView() {

    companion object {
        private val log = LoggerFactory.getLogger(MustacheView::class.java)
    }

    private var compiler: Compiler? = null
    private var charset: String? = null

    fun setCompiler(compiler: Compiler) {
        this.compiler = compiler
    }

    fun setCharset(charset: String) {
        this.charset = charset
    }

    @Throws(Exception::class)
    override fun checkResourceExists(locale: Locale): Boolean {
        return resolveResource() != null
    }

    override fun renderInternal(renderAttributes: MutableMap<String, Any>,
                                contentType: MediaType?,
                                exchange: ServerWebExchange): Mono<Void> {
        val resource = resolveResource()
                       ?: return Mono.error(IllegalStateException("해당 URL의 Mustache template를 찾지 못했습니다. [$url]"))

        log.debug("renderAttributes={}", renderAttributes)

        val dataBuffer = exchange.response.bufferFactory().allocateBuffer()
        try {
            getReader(resource).use { reader ->
                val template = this.compiler!!.compile(reader)
                val charset = contentType?.charset ?: defaultCharset

                OutputStreamWriter(dataBuffer.asOutputStream(), charset).use { writer ->
                    template.execute(renderAttributes, writer)
                    writer.flush()
                }
            }
        } catch (e: Throwable) {
            log.error("Error occured in renderInternal.", e)
            return Mono.error<Void>(e)
        }

        return exchange.response.writeWith(Flux.just(dataBuffer))
    }

    private fun resolveResource(): Resource? {
        val resource = applicationContext!!.getResource(url!!)
        return if (resource.exists()) resource else null
    }

    private fun getReader(resource: Resource): Reader {
        return this.charset?.let { InputStreamReader(resource.inputStream, it) }
               ?: InputStreamReader(resource.inputStream)
    }
}
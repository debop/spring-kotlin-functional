/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop4k.functional.web

import com.samskivert.mustache.Mustache
import debop4k.functional.locale
import org.slf4j.LoggerFactory
import org.springframework.context.MessageSource
import org.springframework.core.io.ClassPathResource
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.RenderingResponse
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.router
import reactor.core.publisher.toMono
import java.util.*

/**
 * Routes
 * @author debop
 * @since 2018. 2. 17.
 */
class Routes(private val userHandler: UserHandler,
             private val messageSource: MessageSource) {
    companion object {
        private val log = LoggerFactory.getLogger(Routes::class.java)
    }

    fun route(): RouterFunction<ServerResponse> = router {
        accept(MediaType.TEXT_HTML).nest {
            GET("/") { ok().render("index") }
            GET("/sse") { ok().render("sse") }
            GET("/users") { userHandler.findAllView(it) }
        }

        "/api".nest {
            accept(MediaType.APPLICATION_JSON).nest {
                GET("/users") { userHandler.findAll(it) }
            }
            accept(MediaType.TEXT_EVENT_STREAM).nest {
                GET("/users") { userHandler.stream(it) }
            }
        }

        resources("/**", ClassPathResource("static/"))
    }.filter { request, next ->
        next.handle(request).flatMap {
            when (it) {
            // WEB Page 애 Mustache 중 다국어 관련 Rendering 이 필요한 경우
                is RenderingResponse -> {
                    log.debug("RenderingResponse를 wrapping 함. add locale information. locale={}, request={}", request.locale(), request)
                    RenderingResponse
                            .from(it)
                            .modelAttributes(attributes(request.locale(), messageSource))
                            .build()
                }
                else                 -> {
                    log.debug("It is not RenderingResponse, so return wrap to Mono<T>")
                    it.toMono()
                }
            }
        }
    }

    private fun attributes(locale: Locale, messageSource: MessageSource): MutableMap<String, Any> {
        val attrs: MutableMap<String, Any> = mutableMapOf("i18n" to Mustache.Lambda { frag, out ->
            val tokens = frag.execute().split("|")
            log.debug("tokens={}", tokens.joinToString())
            if (tokens.isNotEmpty()) {
                out.write(messageSource.getMessage(tokens[0], tokens.slice(IntRange(1, tokens.size - 1)).toTypedArray(), locale))
            }
        })
        return attrs
    }
}
/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop4k.functional.web.view

import com.samskivert.mustache.Mustache
import com.samskivert.mustache.Mustache.Compiler
import org.slf4j.LoggerFactory
import org.springframework.web.reactive.result.view.AbstractUrlBasedView
import org.springframework.web.reactive.result.view.UrlBasedViewResolver

/**
 * Spring WebFux [ViewResolver] for Mustache
 * @author debop
 * @since 2018. 2. 18.
 */
class MustacheViewResolver(private val compiler: Compiler = Mustache.compiler()) : UrlBasedViewResolver() {

    companion object {
        private val log = LoggerFactory.getLogger(MustacheViewResolver::class.java)
    }

    private var charset: String? = null

    init {
        viewClass = requiredViewClass()
    }

    fun setCharset(charset: String) {
        this.charset = charset
    }

    override fun requiredViewClass(): Class<*> {
        return MustacheView::class.java
    }

    override fun createView(viewName: String): AbstractUrlBasedView {
        log.info("Create View. viewName={}", viewName)

        val view = super.createView(viewName) as MustacheView
        view.setCompiler(this.compiler)
        this.charset?.let { view.setCharset(it) }
        return view
    }
}
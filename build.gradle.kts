import org.jetbrains.kotlin.gradle.plugin.KotlinPluginWrapper
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jetbrains.kotlin.gradle.dsl.Coroutines

plugins {
    val kotlinVersion = "1.2.21"
    id("org.jetbrains.kotlin.jvm") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.spring") version kotlinVersion
    id("io.spring.dependency-management") version "1.0.4.RELEASE"
    id("org.springframework.boot") version "2.0.0.RC1"
    id("org.junit.platform.gradle.plugin") version "1.1.0"
}

repositories {
    mavenCentral()
    jcenter()
    maven("http://repo.spring.io/milestone")
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
            freeCompilerArgs = listOf("-Xjsr305=strict")
        }
    }
}

kotlin {
    experimental.coroutines = Coroutines.ENABLE
}

//dependencyManagement {
//    imports {
//        mavenBom("org.springframework.boot:spring-boot-dependencies:2.0.0.RC1")
//    }
//}

dependencies {

    compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    compile("org.jetbrains.kotlin:kotlin-reflect")

    compile("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:0.22.2")

    compile("com.fasterxml.jackson.module:jackson-module-kotlin")
    compile("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")

    compile("org.springframework.boot:spring-boot-starter-webflux")
    compile("com.samskivert:jmustache")

    compile("io.reactivex.rxjava2:rxjava:2.1.9")
    compile("io.reactivex.rxjava2:rxkotlin:2.2.0")

    compile("org.slf4j:slf4j-api")
    compile("ch.qos.logback:logback-classic")

    compile("org.springframework.boot:spring-boot-starter-test") {
        exclude(module = "junit")
    }
    testCompile("io.projectreactor:reactor-test")

    testCompile("org.junit.platform:junit-platform-launcher:1.1.0")
    testCompile("org.junit.jupiter:junit-jupiter-api:5.1.0")
    testCompile("org.junit.jupiter:junit-jupiter-engine:5.1.0")
}

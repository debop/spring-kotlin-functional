/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop4k.functional.web

import debop4k.functional.User
import debop4k.functional.formatDate
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.reactor.flux
import org.slf4j.LoggerFactory
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.body
import org.springframework.web.reactive.function.server.bodyToServerSentEvents
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Duration
import java.time.LocalDate

class UserHandler {
    companion object {
        private val log = LoggerFactory.getLogger(UserHandler::class.java)
    }

    //    private val users = Flux.just(
    //            User("Foo", "Foo", LocalDate.now().minusDays(1)),
    //            User("Bar", "Bar", LocalDate.now().minusDays(10)),
    //            User("Sam", "Sam", LocalDate.now().minusDays(100)))

    private val users = flux(CommonPool) {
        log.debug("Sending Foo...")
        delay(10)
        send(User("Foo", "Foo", LocalDate.now().minusDays(1)))
        log.debug("Sending Bar...")
        delay(10)
        send(User("Bar", "Bar", LocalDate.now().minusDays(10)))
        delay(10)
        log.debug("Sending Sam...")
        send(User("Sam", "Sam", LocalDate.now().minusDays(100)))
    }

    private val userStream = users.repeat()
            .zipWith(Flux.interval(Duration.ofMillis(100)))
            .map { it.t1 }

    fun findAll(req: ServerRequest): Mono<ServerResponse> = ok().body(users)

    fun findAllView(req: ServerRequest): Mono<ServerResponse> =
            ok().render("users", mapOf("users" to users.map { it.toDto() }))

    fun stream(req: ServerRequest): Mono<ServerResponse> =
            ok().bodyToServerSentEvents(userStream)
}


class UserDto(val firstName: String, val lastName: String, val birthDate: String)

fun User.toDto() = UserDto(firstName, lastName, birthDate.formatDate())
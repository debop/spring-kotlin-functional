/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop4k.functional

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToFlux
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.test.test
import java.time.LocalDate

@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class IntegrationTests(@LocalServerPort port: Int) {

    private val client = WebClient.create("http://localhost:$port")

    @Test
    fun `find all users on JSON REST endpoint`() {
        client.get()
                .uri("/api/users")
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux<User>()
                .test()
                .expectNextMatches { it.firstName == "Foo" && it.lastName == "Foo" && it.birthDate.isBefore(LocalDate.now()) }
                .expectNextMatches { it.firstName == "Bar" && it.lastName == "Bar" && it.birthDate.isBefore(LocalDate.now()) }
                .expectNextMatches { it.firstName == "Sam" && it.lastName == "Sam" && it.birthDate.isBefore(LocalDate.now()) }
                .verifyComplete()
    }

    @Test
    fun `find all users on HTML page`() {
        client.get()
                .uri("/users")
                .accept(MediaType.TEXT_HTML)
                .retrieve()
                .bodyToMono<String>()
                .test()
                .expectNextMatches { it.contains("Foo") && it.contains("Bar") && it.contains("Sam") }
                .verifyComplete()
    }

    @Test fun `Retrieve a stream of users via Server-Sent-Events`() {
        client.get()
                .uri("/api/users")
                .accept(MediaType.TEXT_EVENT_STREAM)
                .retrieve()
                .bodyToFlux<User>()
                .test()
                .expectNextMatches { it.firstName == "Foo" && it.lastName == "Foo" && it.birthDate.isBefore(LocalDate.now()) }
                .expectNextMatches { it.firstName == "Bar" && it.lastName == "Bar" && it.birthDate.isBefore(LocalDate.now()) }
                .expectNextMatches { it.firstName == "Sam" && it.lastName == "Sam" && it.birthDate.isBefore(LocalDate.now()) }
                .expectNextMatches { it.firstName == "Foo" && it.lastName == "Foo" && it.birthDate.isBefore(LocalDate.now()) }
                .expectNextMatches { it.firstName == "Bar" && it.lastName == "Bar" && it.birthDate.isBefore(LocalDate.now()) }
                .expectNextMatches { it.firstName == "Sam" && it.lastName == "Sam" && it.birthDate.isBefore(LocalDate.now()) }
                .expectNextMatches { it.firstName == "Foo" && it.lastName == "Foo" && it.birthDate.isBefore(LocalDate.now()) }
                .expectNextMatches { it.firstName == "Bar" && it.lastName == "Bar" && it.birthDate.isBefore(LocalDate.now()) }
                .expectNextMatches { it.firstName == "Sam" && it.lastName == "Sam" && it.birthDate.isBefore(LocalDate.now()) }
                .expectNextMatches { it.firstName == "Foo" && it.lastName == "Foo" && it.birthDate.isBefore(LocalDate.now()) }
                .expectNextMatches { it.firstName == "Bar" && it.lastName == "Bar" && it.birthDate.isBefore(LocalDate.now()) }
                .expectNextMatches { it.firstName == "Sam" && it.lastName == "Sam" && it.birthDate.isBefore(LocalDate.now()) }
                .thenCancel()
                .verify()
    }
}


/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:JvmName("Extensions")

package debop4k.functional

import org.springframework.web.reactive.function.server.ServerRequest
import java.time.LocalDate
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField
import java.util.*

fun ServerRequest.locale(): Locale =
        this.headers().asHttpHeaders().acceptLanguageAsLocales.firstOrNull() ?: Locale.KOREA!!

fun LocalDate.formatDate(): String = format(koreanDateFormatter)


private val daysLookup: Map<Long, String> =
        (1 .. 31).map { it.toLong() to getOrdinal(it) }.toMap()

private fun getOrdinal(n: Int): String = when {
    n in 11 .. 13 -> "${n}th"
    n % 10 == 1   -> "${n}st"
    n % 10 == 2   -> "${n}nd"
    n % 10 == 3   -> "${n}rd"
    else          -> "${n}th"
}

private val koreanDateFormatter =
        DateTimeFormatterBuilder()
                .appendPattern("MMMM")
                .appendLiteral(" ")
                .appendText(ChronoField.DAY_OF_MONTH, daysLookup)
                .appendLiteral(" ")
                .appendPattern("yyyy")
                .toFormatter(Locale.KOREAN)
